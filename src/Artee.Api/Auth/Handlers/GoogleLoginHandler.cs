﻿using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.Framework.Configuration;
using Newtonsoft.Json;

namespace Artee.Api.Auth.Handlers
{
    public class GoogleLoginHandler : IExternalLoginHandler
    {
        private const string TokenEndpoint = "https://accounts.google.com/o/oauth2/token";
        private const string ProfileInfoEndpoint = "https://www.googleapis.com/userinfo/v2/me";

        private readonly string _clientId;
        private readonly string _clientSecret;

        public GoogleLoginHandler(IConfiguration configuration)
        {
            _clientId = configuration[ConfigurationNames.GoogleClientId];
            _clientSecret = configuration[ConfigurationNames.GoogleClientSecret];
        }

        public async Task<ExternalProfile> GetProfile(string accessCode, string redirectUri)
        {
            using (var httpClient = new HttpClient())
            {
                var tokenChallangeResponse =
                    await httpClient.PostAsync(TokenEndpoint, CreateTokenChallengeBody(accessCode, redirectUri));
                var tokenResult = await tokenChallangeResponse.Content.ReadAsAsync<TokenResult>();
                var profileRequest = new HttpRequestMessage(HttpMethod.Get, ProfileInfoEndpoint);
                profileRequest.Headers.Authorization =
                    AuthenticationHeaderValue.Parse($"Bearer {tokenResult.AccessToken}");
                var profileResponse = await httpClient.SendAsync(profileRequest);
                var googleProfile = await profileResponse.Content.ReadAsAsync<ProfileResult>();
                return new ExternalProfile
                {
                    Email = googleProfile.Email,
                    Name = googleProfile.Name,
                    AvatarUri = googleProfile.Picture
                };
            }
        }


        private FormUrlEncodedContent CreateTokenChallengeBody(string accessCode, string redirectUri)
        {
            var keyValues = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("code", accessCode),
                new KeyValuePair<string, string>("client_id", _clientId),
                new KeyValuePair<string, string>("client_secret", _clientSecret),
                new KeyValuePair<string, string>("redirect_uri", redirectUri),
                new KeyValuePair<string, string>("grant_type", "authorization_code")
            };
            return new FormUrlEncodedContent(keyValues);
        }

        private class TokenResult
        {
            [JsonProperty("access_token")]
            public string AccessToken { get; set; }

            [JsonProperty("token_type")]
            public string TokenType { get; set; }
        }

        private class ProfileResult
        {
            public string Email { get; set; }

            public string Name { get; set; }

            public string Picture { get; set; }
        }
    }
}