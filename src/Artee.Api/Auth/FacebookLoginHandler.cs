﻿using System;
using System.Threading.Tasks;
using Microsoft.Framework.Configuration;

namespace Artee.Api.Auth.Handlers
{
    public class FacebookLoginHandler : IExternalLoginHandler
    {
        private IConfiguration _configuration;

        public FacebookLoginHandler(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public Task<ExternalProfile> GetProfile(string accessCode, string redirectUri)
        {
            throw new NotImplementedException();
        }
    }
}