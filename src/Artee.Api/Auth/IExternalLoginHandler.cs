﻿using System.Threading.Tasks;

namespace Artee.Api.Auth
{
    public interface IExternalLoginHandler
    {
        Task<ExternalProfile> GetProfile(string accessCode, string redirectUri);
    }
}