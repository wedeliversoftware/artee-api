﻿using System;
using System.IdentityModel.Tokens;

namespace Artee.Api.Auth
{
    public class TokenOptions
    {
        public SigningCredentials SigningCredentials { get; set; }

        public string Audience { get; set; }

        public string Issuer { get; set; }

        public TimeSpan Validity { get; set; }
    }
}