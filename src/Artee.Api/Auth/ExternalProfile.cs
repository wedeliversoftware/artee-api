﻿namespace Artee.Api.Auth
{
    public class ExternalProfile
    {
        public string Name { get; set; }

        public string Email { get; set; }

        public string AvatarUri { get; set; }
    }
}