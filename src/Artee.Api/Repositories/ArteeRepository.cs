﻿using Microsoft.Azure.Documents.Client;
using Microsoft.Framework.Configuration;

namespace Artee.Api.Repositories
{
    public class ArteeRepository : DocumentDbRepository<Documents.Artee>
    {
        public ArteeRepository(DocumentClient documentClient, IConfiguration configuration) : base(documentClient, configuration)
        {
        }
    }
}