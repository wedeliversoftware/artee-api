﻿using Artee.Api.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Framework.Configuration;

namespace Artee.Api.Repositories
{
    public class PhotoRepository : DocumentDbRepository<Photo>
    {
        public PhotoRepository(DocumentClient documentClient, IConfiguration configuration) : base(documentClient, configuration)
        {
        }
    }
}