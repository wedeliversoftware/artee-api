﻿using Artee.Api.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Framework.Configuration;

namespace Artee.Api.Repositories
{
    public class CommentRepository : DocumentDbRepository<Documents.Comment>
    {
        public CommentRepository(DocumentClient documentClient, IConfiguration configuration) : base(documentClient, configuration)
        {
        }
    }
}