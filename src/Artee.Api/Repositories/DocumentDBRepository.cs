﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Artee.Api.Documents;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Framework.Configuration;
using Microsoft.Azure.Documents.Linq;

namespace Artee.Api.Repositories
{
    public abstract class DocumentDbRepository<TDocument> : IDocumentDbRepository<TDocument> where TDocument : DocumentBase
    {
        private DocumentCollection _collection;
        protected readonly DocumentClient DocumentClient;
        protected readonly IConfiguration Configuration;

        protected DocumentDbRepository(DocumentClient documentClient, IConfiguration configuration)
        {
            DocumentClient = documentClient;
            Configuration = configuration;
        }

        public DocumentCollection Collection
        {
            get
            {
                if (_collection == null)
                {
                    _collection = ReadCollection();
                }

                return _collection;
            }
        }

        public IEnumerable<TDocument> GetItems(Expression<Func<TDocument, bool>> predicate)
        {
            return DocumentClient
                .CreateDocumentQuery<TDocument>(Collection.DocumentsLink)
                .Where(predicate)
                .AsEnumerable();
        }

        public virtual TDocument GetById(string id)
        {
            return DocumentClient
                .CreateDocumentQuery<TDocument>(Collection.DocumentsLink)
                .AsEnumerable()
                .SingleOrDefault(a => a.Id == id);
        }

        public TDocument UpdateItem(TDocument document)
        {
            var doc = DocumentClient.CreateDocumentQuery(Collection.SelfLink).AsEnumerable().First(d => d.Id == document.Id);
            dynamic updatedDoc = DocumentClient.ReplaceDocumentAsync(doc.SelfLink, document).Result.Resource;
            TDocument result = updatedDoc;
            return result;
        }

        public async Task<ResourceResponse<Document>> AddItem(TDocument document)
        {
            document.Id = Guid.NewGuid().ToString();
            document.CreationDate = DateTime.UtcNow;

            return await DocumentClient.CreateDocumentAsync(Collection.DocumentsLink, document);
        }

        protected DocumentCollection ReadCollection()
        {
            return DocumentClient
                .CreateDocumentCollectionQuery(Configuration[ConfigurationNames.DatabaseUri])
                .Where(c => c.Id == GetCollectionName())
                .AsEnumerable()
                .FirstOrDefault();
        }

        private string GetCollectionName()
        {
            return $"{typeof(TDocument).Name}Collection";
        }
    }
}