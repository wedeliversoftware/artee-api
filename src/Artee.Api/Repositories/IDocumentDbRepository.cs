﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Artee.Api.Documents;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;

namespace Artee.Api.Repositories
{
    public interface IDocumentDbRepository<TDocument> where TDocument : DocumentBase
    {
        DocumentCollection Collection { get; }

        Task<ResourceResponse<Document>> AddItem(TDocument document);

        IEnumerable<TDocument> GetItems(Expression<Func<TDocument, bool>> predicate);

        TDocument GetById(string id);

        TDocument UpdateItem(TDocument document);
    }
}