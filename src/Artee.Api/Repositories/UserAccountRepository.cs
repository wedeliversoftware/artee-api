using Microsoft.Azure.Documents.Client;
using Microsoft.Framework.Configuration;

namespace Artee.Api.Repositories
{
    public class UserAccountRepository : DocumentDbRepository<Documents.UserAccount>
    {
        public UserAccountRepository(DocumentClient documentClient, IConfiguration configuration) : base(documentClient, configuration)
        {
        }
    }
}