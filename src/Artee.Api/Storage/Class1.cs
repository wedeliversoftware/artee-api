﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNet.Http;
using Microsoft.Framework.Configuration;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace Artee.Api.Storage
{
    public class BlobStorage : IBlobStorage
    {
        private readonly IConfiguration _configuration;

        public BlobStorage(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public string Upload(IFormFile formFile, string extension)
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(_configuration[ConfigurationNames.StorageConnectionString]);
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer container = blobClient.GetContainerReference("picturesblob");

            var fileName = Guid.NewGuid().ToString().Replace("-", "") + extension;
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(fileName);
            blockBlob.UploadFromStream(formFile.OpenReadStream());
            return blockBlob.Uri.ToString();
        }
    }
}