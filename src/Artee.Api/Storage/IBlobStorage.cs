using Microsoft.AspNet.Http;

namespace Artee.Api.Storage
{
    public interface IBlobStorage
    {
        string Upload(IFormFile formFile, string extension);
    }
}