﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Artee.Api.Artee;
using Artee.Api.ArteeWithComments;
using Artee.Api.Documents;
using Artee.Api.Repositories;
using Artee.Api.Storage;
using Artee.Api.Users;
using MediatR;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Mvc;

namespace Artee.Api.Controllers
{
    [Route("api/")]
    public class ArteeController : Controller
    {
        private readonly IMediator _mediator;
        private readonly IUserHelper _userHelper;
        private readonly IBlobStorage _blobStorage;
        private readonly IDocumentDbRepository<Documents.Artee> _arteeRepository;
        private readonly IDocumentDbRepository<Photo> _photoRepository;

        public ArteeController(IMediator mediator, IUserHelper userHelper, IBlobStorage blobStorage, IDocumentDbRepository<Documents.Artee> arteeRepository, IDocumentDbRepository<Photo> photoRepository)
        {
            _mediator = mediator;
            _userHelper = userHelper;
            _blobStorage = blobStorage;
            _arteeRepository = arteeRepository;
            _photoRepository = photoRepository;
        }

        [HttpGet]
        [Route("artees")]
        public async Task<IEnumerable<ArteeQueryResult>> GetArtees(QueryArtees query)
        {
            return await _mediator.SendAsync(query);
        }

        [HttpGet]
        [Route("artee/{id}")]
        public async Task<ArteeWithComments.ArteeWithComments> GetArtee(string id)
        {
            var query = new QueryArteeWithComments { ArteeId = id };
            return await _mediator.SendAsync(query);
        }

        [HttpPost]
        [Route("artee")]
        [Authorize("Bearer")]
        public async Task<string> CreateArtee([FromBody] CreateArteeCommand createCommand)
        {
            createCommand.Submitter = _userHelper.GetCurrentUserProfile(this);
            return await _mediator.SendAsync(createCommand);
        }

        [HttpPost]
        [Route("artee/photo")]
        [Authorize("Bearer")]
        public async Task<string> UploadArteePhoto()
        {
            var photo = new Documents.Photo();
            
            string arteeId = Request.Form["artee_id"].ToString().Replace(@"""", "");
            string extension = Path.GetExtension(Request.Form["filename"]);
            photo.ImageUri = _blobStorage.Upload(Request.Form.Files["file"], extension);
            photo.MiniImageUri = photo.ImageUri;
            photo.SubmitterHandle = _userHelper.GetCurrentUserProfile(this).Handle;

            await _photoRepository.AddItem(photo);
            var artee = _arteeRepository.GetById(arteeId);
            artee.Photos.Add(photo);
            _arteeRepository.UpdateItem(artee);

            return photo.ImageUri;
        }
    }
}