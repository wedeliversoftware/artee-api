﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Artee.Api.Artee;
using Artee.Api.Documents;
using Artee.Api.Repositories;
using Microsoft.AspNet.Mvc;

namespace Artee.Api.Controllers
{
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        private readonly IDocumentDbRepository<UserAccount> _userRepository;
        private readonly IDocumentDbRepository<Documents.Comment> _commentsRepository;
        private readonly IDocumentDbRepository<Documents.Artee> _arteeRepository;

        public UserController(IDocumentDbRepository<Documents.UserAccount> userRepository, 
            IDocumentDbRepository<Documents.Comment> commentsRepository, 
            IDocumentDbRepository<Documents.Artee> arteeRepository)
        {
            _userRepository = userRepository;
            _commentsRepository = commentsRepository;
            _arteeRepository = arteeRepository;
        }

        [HttpGet("{handle}")]
        public ActionResult Get(string handle)
        {
            var user = _userRepository.GetItems(u => u.Profile.Handle == handle).FirstOrDefault();
            if (user == null)
            {
                return HttpNotFound();
            }

            return Json(user.Profile);
        }

        [HttpGet("{handle}/comments")]
        public ActionResult GetComments(string handle)
        {
            var comments = _commentsRepository.GetItems(c => c.Submitter.Handle == handle).ToList();
            return Json(comments);
        }

        [HttpGet("{handle}/artees")]
        public ActionResult GetArtees(string handle)
        {
            return Json(_arteeRepository
                .GetItems(a => a.Submitter.Handle == handle)
                .Select(a => new ArteeQueryResult
                {
                    Id = a.Id,
                    CommentsCount = _commentsRepository.GetItems(c => c.ArteeId == a.Id).Count(),
                    Name = a.Name,
                    Place = a.Place,
                    Description = a.Description,
                    Rating = a.Rating,
                    Longitude = a.Location.Position.Longitude,
                    Latitude = a.Location.Position.Latitude,
                    Photos = (a.Photos ?? new List<Photo>()).Take(1).ToList()
                })
                .ToList());
        }
    }
}