﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Artee.Api.Artee;
using Artee.Api.Comment;
using Artee.Api.Users;
using MediatR;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Mvc;

namespace Artee.Api.Controllers
{
    [Route("api/[controller]")]
    public class CommentController : Controller
    {
        private readonly IMediator _mediator;
        private readonly IUserHelper _userHelper;

        public CommentController(IMediator mediator, IUserHelper userHelper)
        {
            _mediator = mediator;
            _userHelper = userHelper;
        }

        [HttpGet]
        public async Task<IEnumerable<Documents.Comment>> GetArtees(QueryComments query)
        {
            return await _mediator.SendAsync(query);
        }

        [HttpPost]
        [Authorize("Bearer")]
        public async Task CreateComment([FromBody] CreateCommentCommand command)
        {
            command.Submitter = _userHelper.GetCurrentUserProfile(this);
            await _mediator.SendAsync(command);
        }
    }
}