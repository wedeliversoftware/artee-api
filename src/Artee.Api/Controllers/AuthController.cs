﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using Artee.Api.Auth;
using Artee.Api.Auth.Handlers;
using Artee.Api.Documents;
using Artee.Api.Models;
using Artee.Api.Repositories;
using Artee.Api.Users;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Mvc;
using Microsoft.Azure.Documents;
using Microsoft.Framework.Configuration;

namespace Artee.Api.Controllers
{
    [Route("api/[controller]")]
    public class AuthController : Controller
    {
        private readonly IDictionary<string, IExternalLoginHandler> _handlers;
        private readonly TokenOptions _tokenOptions;
        private readonly IDocumentDbRepository<UserAccount> _repo;
        private readonly IUserHelper _userHelper;

        public AuthController(IConfiguration configuration, TokenOptions tokenOptions, IDocumentDbRepository<UserAccount> repo, IUserHelper userHelper)
        {
            _tokenOptions = tokenOptions;
            _repo = repo;
            _userHelper = userHelper;
            _handlers = new Dictionary<string, IExternalLoginHandler>
        {
                {ExternalProviders.Google, new GoogleLoginHandler(configuration)},
                {ExternalProviders.Facebook, new FacebookLoginHandler(configuration)}
            };
        }

        [HttpPost("loginExternal")]
        public async Task<ActionResult> LoginExternal([FromBody] ExternalLoginData loginData)
        {
            if (string.IsNullOrWhiteSpace(loginData.AccessCode) || string.IsNullOrEmpty(loginData.Provider) ||
                string.IsNullOrWhiteSpace(loginData.RedirectUri))
            {
                return HttpUnauthorized();
            }

            var profile = await _handlers[loginData.Provider].GetProfile(loginData.AccessCode, loginData.RedirectUri);
            if (profile == null)
            {
                return HttpUnauthorized();
            }

            var userAccount = GetOrCreateUser(profile, loginData.Provider);
            return Content(GenerateBearerToken(await userAccount));
        }

        [HttpGet("whoami")]
        [Authorize("Bearer")]
        public UserProfile GetCurrentUser()
        {
            return _userHelper.GetCurrentUserProfile(this);
        }

        private async Task<UserAccount> GetOrCreateUser(ExternalProfile externalProfile, string provider)
        {
            var account = _repo
                .GetItems(x => x.Login == externalProfile.Email)
                .SingleOrDefault();
            if (account != null) return account;
            var userProfile = new UserProfile
            {
                AvatarUri = externalProfile.AvatarUri,
                Name = externalProfile.Name,
                Bio = string.Empty,
            };

            userProfile.Handle = userProfile.Name.RemoveAccent().ToLower().Replace(" ", string.Empty);
            var newAccount = new UserAccount {Login = externalProfile.Email, Provider = provider, Profile = userProfile};
            await _repo.AddItem(newAccount);

            return newAccount;
        }

        private string GenerateBearerToken(UserAccount userAccount)
        {
            var handler = new JwtSecurityTokenHandler();

            var identity = new ClaimsIdentity(
                new GenericIdentity(userAccount.Login),
                new[] {new Claim("Name", userAccount.Profile.Name, ClaimValueTypes.String)});

            var securityToken = handler.CreateToken(
                _tokenOptions.Issuer,
                _tokenOptions.Audience,
                signingCredentials: _tokenOptions.SigningCredentials,
                subject: identity,
                expires: DateTime.UtcNow.Add(_tokenOptions.Validity));

            return handler.WriteToken(securityToken);
        }
    }
}