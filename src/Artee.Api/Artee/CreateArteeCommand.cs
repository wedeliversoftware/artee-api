﻿using System.Collections.Generic;
using Artee.Api.Documents;
using MediatR;
using Newtonsoft.Json;

namespace Artee.Api.Artee
{
    public class CreateArteeCommand : IAsyncRequest<string>
    {
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "longitude")]
        public double Longitude { get; set; }

        [JsonProperty(PropertyName = "latitude")]
        public double Latitude { get; set; }

        [JsonProperty(PropertyName = "place")]
        public Place Place { get; set; }

        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        public UserProfile Submitter { get; set; }

        public IList<Photo> Photos { get; set; }
    }
}