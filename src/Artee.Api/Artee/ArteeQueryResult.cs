﻿using System.Collections.Generic;
using Artee.Api.Documents;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Artee.Api.Artee
{
    public class ArteeQueryResult
    {
        public string Id { get; set; }

        public string Name { get; set; } 

        public string Description { get; set; }

        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate)]
        public int Rating { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public IList<Photo> Photos { get; set; }

        public Place Place { get; set; }

        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate)]
        public int CommentsCount { get; set; }
    }
}