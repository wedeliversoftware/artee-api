﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Artee.Api.Documents;
using Artee.Api.Repositories;
using MediatR;
using Microsoft.Azure.Documents.Spatial;

namespace Artee.Api.Artee.Handlers
{
    public class QueryArteesHandler : IAsyncRequestHandler<QueryArtees, IEnumerable<ArteeQueryResult>>
    {
        private readonly IDocumentDbRepository<Documents.Artee> _arteeRepository;
        private readonly IDocumentDbRepository<Documents.Comment> _commentsRepository;

        public QueryArteesHandler(IDocumentDbRepository<Documents.Artee> arteeRepository, IDocumentDbRepository<Documents.Comment> commentsRepository)
        {
            if (arteeRepository == null) throw new ArgumentNullException(nameof(arteeRepository));

            _arteeRepository = arteeRepository;
            _commentsRepository = commentsRepository;
        }

        public async Task<IEnumerable<ArteeQueryResult>> Handle(QueryArtees message)
        {
            Expression<Func<Documents.Artee, bool>> filterExpression = a => true;
            if (message.Distance.HasValue && message.Latitude.HasValue && message.Longitude.HasValue)
            {
                var location = new Point(message.Longitude.Value, message.Latitude.Value);
                filterExpression = a => a.Location.Distance(location) < message.Distance.Value;
            }

            return await Task.FromResult(_arteeRepository
                .GetItems(filterExpression)
                .OrderByDescending(a => a.Rating)
                .Select(a => new ArteeQueryResult
                {
                    Id = a.Id,
                    CommentsCount = _commentsRepository.GetItems(c => c.ArteeId == a.Id).Count(),
                    Name = a.Name,
                    Place = a.Place,
                    Description = a.Description,
                    Rating = a.Rating,
                    Longitude = a.Location.Position.Longitude,
                    Latitude = a.Location.Position.Latitude,
                    Photos = (a.Photos ?? new List<Photo>()).Take(1).ToList()
                }));
        }

       
    }
}