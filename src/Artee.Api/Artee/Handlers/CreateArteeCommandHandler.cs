﻿using System;
using System.Threading.Tasks;
using Artee.Api.Documents;
using Artee.Api.Repositories;
using MediatR;

namespace Artee.Api.Artee.Handlers
{
    public class CreateArteeCommandHandler : IAsyncRequestHandler<CreateArteeCommand, string>
    {
        private readonly IDocumentDbRepository<Documents.Artee> _arteeRepository;
        private readonly IDocumentDbRepository<Photo> _photosRepository;

        public CreateArteeCommandHandler(IDocumentDbRepository<Documents.Artee> arteeRepository, IDocumentDbRepository<Photo> photosRepository)
        {
            if (arteeRepository == null) throw new ArgumentNullException(nameof(arteeRepository));

            _arteeRepository = arteeRepository;
            _photosRepository = photosRepository;
        }

        public async Task<string> Handle(CreateArteeCommand message)
        {
            var newArtee = new Documents.Artee(message.Name, message.Longitude, message.Latitude, description: message.Description);
            newArtee.Place = message.Place;
            newArtee.Submitter = message.Submitter;

            foreach (Photo photo in newArtee.Photos)
            {
                await _photosRepository.AddItem(photo);
            }

            await _arteeRepository.AddItem(newArtee);
            return newArtee.Id;
        }
    }
}