﻿using System.Collections.Generic;
using MediatR;

namespace Artee.Api.Artee
{
    public class QueryArtees : IAsyncRequest<IEnumerable<ArteeQueryResult>>
    {
        public int? Distance { get; set; }

        public double? Latitude { get; set; }

        public double? Longitude { get; set; }
    }
}