namespace Artee.Api.Models
{
    public class ExternalLoginData
    {
        public string AccessCode { get; set; }

        public string Provider { get; set; }

        public string RedirectUri { get; set; }
    }
}