using Artee.Api.Documents;
using Microsoft.AspNet.Mvc;

namespace Artee.Api.Users
{
    public interface IUserHelper
    {
        UserProfile GetCurrentUserProfile(Controller controller);
    }
}