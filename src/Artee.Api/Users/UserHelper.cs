using System.Linq;
using Artee.Api.Documents;
using Artee.Api.Repositories;
using Microsoft.AspNet.Mvc;

namespace Artee.Api.Users
{
    public class UserHelper : IUserHelper
    {
        private readonly IDocumentDbRepository<UserAccount> _repo;

        public UserHelper(IDocumentDbRepository<UserAccount> repo)
        {
            _repo = repo;
        }

        public UserProfile GetCurrentUserProfile(Controller controller)
        {
            return _repo.GetItems(acc => acc.Login == controller.User.Identity.Name).Single().Profile;
        }
    }
}