﻿using System;
using Newtonsoft.Json;

namespace Artee.Api.Documents
{
    public abstract class DocumentBase
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "creationDate")]
        public DateTime CreationDate { get; set; }
    }
}