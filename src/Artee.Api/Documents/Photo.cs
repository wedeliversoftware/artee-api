﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Artee.Api.Documents
{
    public class Photo : DocumentBase
    {
        [JsonProperty("image_uri")]
        public string ImageUri { get; set; }

        [JsonProperty("mini_image_uri")]
        public string MiniImageUri { get; set; }

        [JsonProperty("submitter_handle")]
        public string SubmitterHandle { get; set; }
    }
}