﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Artee.Api.Documents
{
    public class UserProfile
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("handle")]
        public string Handle { get; set; }

        [JsonProperty("bio")]
        public string Bio { get; set; }

        [JsonProperty("avatarUri")]
        public string AvatarUri { get; set; }    
    }
}