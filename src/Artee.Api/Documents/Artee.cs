﻿using System.Collections.Generic;
using System.Threading;
using Microsoft.Azure.Documents.Spatial;
using Newtonsoft.Json;

namespace Artee.Api.Documents
{
    public class Artee : DocumentBase
    {
        public Artee(string name, double longitude, double latitude, int rating = 0, string description = null)
        {
            Name = name;
            Location = new Point(longitude, latitude);
            Rating = rating;
            Description = description;
            Place = new Place();
            Photos = new List<Photo>();
        }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "location")]
        public Point Location { get; set; }

        [JsonProperty(PropertyName = "place")]
        public Place Place { get; set; }

        [JsonProperty(PropertyName = "rating")]
        public int Rating { get; set; }

        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "photos")]
        public IList<Photo> Photos { get; set; }

        [JsonProperty(PropertyName = "submitter")]
        public UserProfile Submitter { get; set; }
    }

    public class Place
    {
        [JsonProperty("place_name")]
        public string Name { get; set; }

        [JsonProperty("place_description")]
        public string Description { get; set; }
    }
}