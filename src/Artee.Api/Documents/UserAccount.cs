using Newtonsoft.Json;

namespace Artee.Api.Documents
{
    public class UserAccount : DocumentBase
    {

        [JsonProperty("login")]
        public string Login { get; set; }

        [JsonProperty("profile")]
        public UserProfile Profile { get; set; }

        [JsonProperty("provider")]
        public string Provider { get; set; }
    }
}