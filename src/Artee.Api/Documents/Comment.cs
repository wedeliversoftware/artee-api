﻿using Newtonsoft.Json;

namespace Artee.Api.Documents
{
    public class Comment : DocumentBase
    {
        [JsonProperty(PropertyName = "content")]
        public string Content { get; set; }

        [JsonProperty(PropertyName = "arteeId")]
        public string ArteeId { get; set; }

        [JsonProperty(PropertyName = "submitter")]
        public UserProfile Submitter { get; set; }
    }
}