﻿using Artee.Api.Documents;
using MediatR;
using Newtonsoft.Json;

namespace Artee.Api.Comment
{
    public class CreateCommentCommand : IAsyncRequest
    {
        [JsonProperty("artee_id")]
        public string ArteeId { get; set; }

        public string Comment { get; set; }

        public UserProfile Submitter { get; set; }
    }
}