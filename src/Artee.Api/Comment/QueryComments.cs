﻿using System.Collections.Generic;
using MediatR;

namespace Artee.Api.Comment
{
    public class QueryComments : IAsyncRequest<IEnumerable<Documents.Comment>>
    {
        public string ArteeId { get; set; }
    }
}