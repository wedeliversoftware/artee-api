﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Artee.Api.Repositories;
using MediatR;

namespace Artee.Api.Comment.Handlers
{
    public class QueryCommentsHandler : IAsyncRequestHandler<QueryComments, IEnumerable<Documents.Comment>>
    {
        private readonly IDocumentDbRepository<Documents.Comment> _repository;

        public QueryCommentsHandler(IDocumentDbRepository<Documents.Comment> repository)
        {
            if (repository == null) throw new ArgumentNullException(nameof(repository));

            _repository = repository;
        }

        public async Task<IEnumerable<Documents.Comment>> Handle(QueryComments message)
        {
            return await Task.FromResult(_repository.GetItems(c => c.ArteeId == message.ArteeId));
        }
    }
}