﻿using System;
using System.Threading.Tasks;
using Artee.Api.Repositories;
using MediatR;

namespace Artee.Api.Comment.Handlers
{
    public class CreateCommentCommandHandler : AsyncRequestHandler<CreateCommentCommand>
    {
        private readonly IDocumentDbRepository<Documents.Comment> _repository;

        public CreateCommentCommandHandler(IDocumentDbRepository<Documents.Comment> repository)
        {
            if (repository == null) throw new ArgumentNullException(nameof(repository));

            _repository = repository;
        }

        protected override async Task HandleCore(CreateCommentCommand message)
        {
            var newComment = new Documents.Comment
            {
                ArteeId = message.ArteeId,
                Content = message.Comment,
                Submitter = message.Submitter
            };

            await _repository.AddItem(newComment);
        }
    }
}