﻿namespace Artee.Api
{
    public static class ConfigurationNames
    {
        public static string DatabaseUri = "databaseUri";
        public static string DatabaseEndpoint = "databaseEndpoint";
        public static string DatabaseAuthKey = "databaseAuthKey";
        public static string GoogleClientId = "googleClientId";
        public static string GoogleClientSecret = "googleClientSecret";
        public static string FacebookAppId = "facebookAppId";
        public static string FacebookAppSecret = "facebookAppSecret";
        public static string TokenSigningKey = "tokenSigningKey";
        public static string StorageConnectionString = "storageConnectionString";
    }
}