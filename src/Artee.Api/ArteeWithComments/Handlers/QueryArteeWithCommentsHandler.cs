﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Artee.Api.Artee;
using Artee.Api.Repositories;
using MediatR;

namespace Artee.Api.ArteeWithComments.Handlers
{
    public class QueryArteeWithCommentsHandler : IAsyncRequestHandler<QueryArteeWithComments, ArteeWithComments>
    {
        private readonly IDocumentDbRepository<Documents.Artee> _arteeRepository;
        private readonly IDocumentDbRepository<Documents.Comment> _commentRepository;

        public QueryArteeWithCommentsHandler(IDocumentDbRepository<Documents.Artee> arteeRepository, IDocumentDbRepository<Documents.Comment> commentRepository)
        {
            if (arteeRepository == null) throw new ArgumentNullException(nameof(arteeRepository));
            if (commentRepository == null) throw new ArgumentNullException(nameof(commentRepository));

            _arteeRepository = arteeRepository;
            _commentRepository = commentRepository;
        }

        public async Task<ArteeWithComments> Handle(QueryArteeWithComments message)
        {
            var artee = _arteeRepository.GetById(message.ArteeId);
            var comments = _commentRepository.GetItems(c => c.ArteeId == message.ArteeId);
            var arteeWithComments = new ArteeWithComments
            {
                Artee = artee,
                Comments = comments
            };

            return await Task.FromResult(arteeWithComments);
        }
    }
}