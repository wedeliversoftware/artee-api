﻿using System.Collections.Generic;
using MediatR;

namespace Artee.Api.ArteeWithComments
{
    public class QueryArteeWithComments : IAsyncRequest<ArteeWithComments>
    {
         public string ArteeId { get; set; }
    }
}