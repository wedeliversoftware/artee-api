﻿using System.Collections.Generic;
using Artee.Api.Artee;

namespace Artee.Api.ArteeWithComments
{
    public class ArteeWithComments
    {
        public Documents.Artee Artee { get; set; }

        public IEnumerable<Documents.Comment> Comments { get; set; }
    }
}