﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Diagnostics;
using System.IdentityModel.Tokens;
using System.Security.Cryptography;
using Artee.Api.Auth;
using Microsoft.AspNet.Authentication.JwtBearer;
using Microsoft.AspNet.Authorization;
using System.Reflection;
using Artee.Api.Repositories;
using Artee.Api.Storage;
using Artee.Api.Users;
using MediatR;
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Controllers;
using Microsoft.AspNet.Mvc.Formatters;
using Microsoft.Azure.Documents.Client;
using Microsoft.Dnx.Runtime;
using Microsoft.Framework.Configuration;
using Microsoft.Framework.DependencyInjection;
using Microsoft.Framework.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using SimpleInjector;
using SimpleInjector.Diagnostics;
using SimpleInjector.Extensions.ExecutionContextScoping;

namespace Artee.Api
{
    public class Startup
    {
        private const string ConfigFileName = "config.json";
        private const string TokenAudience = "ArteeApplication";
        private const string TokenIssuer = "ArteeApi";
        private const string DefaultCors = "policy";

        private readonly Container _container = new Container();

        public Startup(IHostingEnvironment env)
        {
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddInstance<IControllerActivator>(new SimpleInjectorControllerActivator(_container));

            services.AddAuthorization(auth =>
            {
                auth.AddPolicy("Bearer", new AuthorizationPolicyBuilder()
                    .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme)
                    .RequireAuthenticatedUser().Build());
            });

            services.AddMvc(options =>
            {
                var jsonOutputFormatter = new JsonOutputFormatter();
                jsonOutputFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                jsonOutputFormatter.SerializerSettings.DefaultValueHandling = DefaultValueHandling.Ignore;

                options.OutputFormatters.Clear();
                options.OutputFormatters.Insert(0, jsonOutputFormatter);
            });

            services.Configure<MvcOptions>(options => { options.Filters.Add(new RequireHttpsAttribute()); });
            ConfigureCors(services);
        }

        private void ConfigureCors(IServiceCollection services)
        {
            var policy = new Microsoft.AspNet.Cors.Core.CorsPolicy();
            policy.Headers.Add("*");
            policy.Methods.Add("*");
            policy.Origins.Add("*");
            policy.SupportsCredentials = true;
            services.AddCors(x => x.AddPolicy("policy", policy));
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IApplicationEnvironment appEnv,
            ILoggerFactory loggerFactory)
        {
            app.UseDeveloperExceptionPage();

            InitializeContainer(app);
            ConfigureMediatr();
            RegisterDocumentClient();
            RegisterConfiguration(appEnv);
            RegisterRepositories();
            RegisterStuff();
            RegisterTokenOptions();
            RegisterControllers(app);

            _container.Verify();

            app.UseJwtBearerAuthentication(bearer =>
            {
                bearer.TokenValidationParameters.IssuerSigningKey =
                    GetTokenSigningKey(_container.GetInstance<IConfiguration>());
                bearer.TokenValidationParameters.ValidAudience = TokenAudience;
                bearer.TokenValidationParameters.ValidIssuer = TokenIssuer;
                bearer.TokenValidationParameters.ValidateSignature = true;
                bearer.TokenValidationParameters.ValidateLifetime = true;
            });

            app.Use(async (context, next) =>
            {
                using (_container.BeginExecutionContextScope())
                {
                    await next();
                }
            });

            loggerFactory.MinimumLevel = LogLevel.Information;
            loggerFactory.AddConsole();
            loggerFactory.AddDebug();

            app.UseIISPlatformHandler();
            app.UseStaticFiles();
            app.UseCors(DefaultCors);
            app.UseMvc();
        }

        private void RegisterStuff()
        {
            _container.Register<IUserHelper, UserHelper>();
            _container.Register<IBlobStorage, BlobStorage>();
        }

        private void RegisterRepositories()
        {
            _container.Register(typeof(IDocumentDbRepository<>), new[] { typeof(IDocumentDbRepository<>).Assembly });
        }

        private void RegisterTokenOptions()
        {
            Func<TokenOptions> getOptions = () => new TokenOptions
            {
                SigningCredentials = new SigningCredentials(
                    GetTokenSigningKey(_container.GetInstance<IConfiguration>()),
                    SecurityAlgorithms.RsaSha256Signature,
                    SecurityAlgorithms.Sha256Digest),
                Audience = TokenAudience,
                Issuer = TokenIssuer,
                Validity = TimeSpan.FromDays(14)
            };

            _container.Register(getOptions);
        }

        private static RsaSecurityKey GetTokenSigningKey(IConfiguration configuration)
        {
            var publicAndPrivate = new RSACryptoServiceProvider();
            publicAndPrivate.FromXmlString(configuration[ConfigurationNames.TokenSigningKey]);
            return new RsaSecurityKey(publicAndPrivate.ExportParameters(true));
        }

        private void RegisterDocumentClient()
        {
            Func<DocumentClient> documentClient = () =>
            {
                var config = _container.GetInstance<IConfiguration>();

                return new DocumentClient(new Uri(config[ConfigurationNames.DatabaseEndpoint]),
                    config[ConfigurationNames.DatabaseAuthKey]);
            };

            _container.Register(documentClient, Lifestyle.Scoped);
        }

        private void RegisterConfiguration(IApplicationEnvironment appEnv)
        {
            var configurationBuilder = new ConfigurationBuilder()
                .SetBasePath(appEnv.ApplicationBasePath)
                .AddJsonFile(ConfigFileName);

            Func<IConfiguration> configuration = () => configurationBuilder.Build();
            _container.Register(configuration, Lifestyle.Singleton);
        }

        private void ConfigureMediatr()
        {
            var assemblies = GetAssemblies().ToArray();
            _container.RegisterSingleton<IMediator, Mediator>();
            _container.Register(typeof(IRequestHandler<,>), assemblies);
            _container.Register(typeof(IAsyncRequestHandler<,>), assemblies);
            _container.RegisterCollection(typeof(INotificationHandler<>), assemblies);
            _container.RegisterCollection(typeof(IAsyncNotificationHandler<>), assemblies);
            _container.RegisterSingleton(new SingleInstanceFactory(_container.GetInstance));
            _container.RegisterSingleton(new MultiInstanceFactory(_container.GetAllInstances));
        }

        private static IEnumerable<Assembly> GetAssemblies()
        {
            yield return typeof(IMediator).Assembly;
            yield return typeof(Startup).Assembly;
        }

        private void InitializeContainer(IApplicationBuilder app)
        {
            _container.Options.DefaultScopedLifestyle = new ExecutionContextScopeLifestyle();
        }

        private void RegisterControllers(IApplicationBuilder app)
        {
            var provider = app.ApplicationServices.GetRequiredService<IControllerTypeProvider>();
            foreach (var type in provider.ControllerTypes)
            {
                var registration = Lifestyle.Transient.CreateRegistration(type, _container);
                _container.AddRegistration(type, registration);
                registration.SuppressDiagnosticWarning(DiagnosticType.DisposableTransientComponent,
                    "ASP.NET disposes controllers.");
            }
        }

        internal sealed class SimpleInjectorControllerActivator : IControllerActivator
        {
            private readonly Container _container;

            public SimpleInjectorControllerActivator(Container container)
            {
                _container = container;
            }

            [DebuggerStepThrough]
            public object Create(ActionContext context, Type controllerType)
            {
                return _container.GetInstance(controllerType);
            }
        }
    }
}
